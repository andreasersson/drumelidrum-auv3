/*
 * Copyright 2024 Andreas Ersson
 *
 * This file is part of drumelidrum-auv3.
 *
 * drumelidrum-auv3 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * drumelidrum-auv3 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with drumelidrum-auv3.  If not, see <https://www.gnu.org/licenses/>.
 */

import SwiftUI

private let _fontSizeScale: Double = 0.027

struct MainView: View {
  var parameterTree: ObservableAUParameterGroup
  let curve: Float = 1.0 / 3.0

  static let widthInUnits = 4
  static let heightInUnits = 5
  static let spacing: Double = 5
  static let padding: Double = 5

  var body: some View {
    GeometryReader { geometry in
      VStack(spacing: MainView.spacing) {
        HStack(spacing: MainView.spacing) {
          ParameterPickerView(parameter: parameterTree.global.OneShot)
            .displayNameHidden()
          Rectangle().hidden().frame(maxHeight: 1)
          Rectangle().hidden().frame(maxHeight: 1)
          ParameterView(parameter: parameterTree.global.Gain)
            .parameterStyle(ParameterKnobStyle())
        }

        HStack(spacing: MainView.spacing) {
          ParameterPickerView(parameter: parameterTree.global.Noise)
            .displayNameHidden()
          ParameterTextFieldView(parameter: parameterTree.global.Pitch)
            .compact()
          ParameterView(parameter: parameterTree.global.Tone)
            .parameterStyle(ParameterKnobStyle())
          ParameterView(parameter: parameterTree.global.Resonance)
            .parameterStyle(ParameterKnobStyle())
            .curve(curve)
        }

        HStack(spacing: MainView.spacing) {
          Spacer()
          ParameterTextFieldView(parameter: parameterTree.global.ClosedKey)
            .compact()
          Spacer()
          ParameterTextFieldView(parameter: parameterTree.global.PedalKey)
            .compact()
          Spacer()
          ParameterTextFieldView(parameter: parameterTree.global.OpenKey)
            .compact()
          Spacer()
        }

        HStack(spacing: MainView.spacing) {
          Spacer()
          ParameterView(parameter: parameterTree.global.ClosedGain)
            .parameterStyle(ParameterKnobStyle())
          Spacer()
          ParameterView(parameter: parameterTree.global.PedalGain)
            .parameterStyle(ParameterKnobStyle())
          Spacer()
          ParameterView(parameter: parameterTree.global.OpenGain)
            .parameterStyle(ParameterKnobStyle())
          Spacer()
        }

        HStack(spacing: MainView.spacing) {
          Spacer()
          ParameterView(parameter: parameterTree.global.ClosedDecay)
            .parameterStyle(ParameterKnobStyle())
            .curve(curve)
          Spacer()
          ParameterView(parameter: parameterTree.global.PedalDecay)
            .parameterStyle(ParameterKnobStyle())
            .curve(curve)
          Spacer()
          ParameterView(parameter: parameterTree.global.OpenDecay)
            .parameterStyle(ParameterKnobStyle())
            .curve(curve)
          Spacer()
        }
      }
        .font(.system(size: geometry.size.height * _fontSizeScale))
    }
      .padding(MainView.padding)
      .background(Color.controlBackgroundColor)
      .environment(\.colorScheme, .dark)
  }
}
