/*
 * Copyright 2024 Andreas Ersson
 *
 * This file is part of drumelidrum-auv3.
 *
 * drumelidrum-auv3 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * drumelidrum-auv3 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with drumelidrum-auv3.  If not, see <https://www.gnu.org/licenses/>.
 */

#import "SdKernel.hpp"
#import "Drum.hpp"
#import "ParameterAddresses.h"

#import <CoreMIDI/CoreMIDI.h>

static inline double from_db(double db_value) {
  static const double c = 1.0 / 20.0;
  return pow(10.0, c * db_value);
}

void SdKernel::initialize(int channelCount, double inSampleRate) {
  m_sampleRate = inSampleRate;
  sd_init(m_sampleRate, &m_sd);

  for (const auto& [address, value] : m_parameters) {
    setParameter(address, value);
  }
}

void SdKernel::setMaximumFramesToRender(const AUAudioFrameCount& maxFrames) {
  m_maxFramesToRender = maxFrames;
  m_buffer.resize(m_maxFramesToRender);
}

void SdKernel::setParameter(AUParameterAddress address, AUValue value) {
  m_parameters[address] = value;

  switch (address) {
    case ParameterAddress::LowKey:
      m_low_key = limit_pitch_value(value);
      break;

    case ParameterAddress::HighKey:
      m_high_key = limit_pitch_value(value);
      break;

    case ParameterAddress::OneShot:
      m_gated = (value >= 0.5) ? true : false;
      break;

    case ParameterAddress::Pitch:
      m_pitch = limit_pitch_value(value);
      break;

    case ParameterAddress::Tone:
      sd_set_tone(value, &m_sd);
      break;

    case ParameterAddress::Snappy:
      sd_set_snappy(value, &m_sd);
      break;

    case ParameterAddress::Decay:
      sd_set_decay(value, &m_sd);
      break;

    case ParameterAddress::Gain:
      m_gain = from_db(value);
      m_gain = std::min(std::max(m_gain, 0.0), 8.0);
      break;

    default:
      break;
  }
}

AUValue SdKernel::getParameter(AUParameterAddress address) {
  return m_parameters[address];
}

bool SdKernel::subProcess(AudioBufferList* inBufferList,
                          AudioBufferList* outBufferList,
                          AUAudioFrameCount bufferOffset,
                          AUAudioFrameCount numFramesToProcess,
                          AUEventSampleTime timestamp) {
  float* leftOut = ((float*)outBufferList->mBuffers[0].mData) + bufferOffset;
  float* rightOut = ((float*)outBufferList->mBuffers[1].mData) + bufferOffset;
  bool isSilent = true;

  if (m_bypassed) {
    for (size_t n = 0u; n < numFramesToProcess; ++n) {
      leftOut[n] = 0.0f;
      rightOut[n] = 0.0f;
    }

    return isSilent;
  }

  isSilent = (SD_SILENT_TRUE == m_sd.silent);
  sd_process(m_buffer.data(), numFramesToProcess, &m_sd);
  isSilent &= (SD_SILENT_TRUE == m_sd.silent);

  for (size_t n = 0u; n < numFramesToProcess; ++n) {
    float sample = m_gain * static_cast<float>(m_buffer[n]);
    leftOut[n] = sample;
    rightOut[n] = sample;
  }

  return isSilent;
}

void SdKernel::handleEvent(AUEventSampleTime now, AURenderEvent const* event) {
  switch (event->head.eventType) {
    case AURenderEventParameter: {
      handleParameterEvent(now, event->parameter);
      break;
    }

    case AURenderEventMIDIEventList: {
      handleMIDIEventList(now, &event->MIDIEventsList);
      break;
    }

    case AURenderEventMIDI:
      break;

    default:
      break;
  }
}

void SdKernel::handleMIDIEventList(AUEventSampleTime now, AUMIDIEventList const* midiEvent) {
  auto visitor = [](void* context, MIDITimeStamp timeStamp,
                    MIDIUniversalMessage message) {
    auto kernel = static_cast<SdKernel*>(context);

    switch (message.type) {
      case kMIDIMessageTypeChannelVoice2: {
        kernel->handleMIDI2VoiceMessage(message);
      } break;

      default:
        break;
    }
  };

  MIDIEventListForEachEvent(&midiEvent->eventList, visitor, this);
}

void SdKernel::handleMIDI2VoiceMessage(const struct MIDIUniversalMessage& message) {
  static const double normalize32 =
      1.0 / double(std::numeric_limits<uint32_t>::max());
  static const double normalize16 =
      1.0 / (double)std::numeric_limits<std::uint16_t>::max();

  const auto& note = message.channelVoice2.note;

  switch (message.channelVoice2.status) {
    case kMIDICVStatusNoteOff:
      if (m_gated && in_range(note.number, m_low_key, m_high_key) &&
          (m_currentNote == note.number)) {
        m_currentNote = -1;
        sd_choke(&m_sd);
      }
      break;

    case kMIDICVStatusNoteOn: {
      int pitch = 0;
      if (in_range(note.number, m_low_key, m_high_key, m_pitch, pitch)) {
        m_currentNote = note.number;
        sd_set_gain((double)message.channelVoice2.note.velocity * normalize16,
                    &m_sd);
        sd_set_frequency(m_pitchToFrequency.frequency(pitch), &m_sd);
        sd_note_on(&m_sd);
      }
      break;
    }

    default:
      break;
  }
}
