/*
 * Copyright 2024 Andreas Ersson
 *
 * This file is part of drumelidrum-auv3.
 *
 * drumelidrum-auv3 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * drumelidrum-auv3 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with drumelidrum-auv3.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef BD_KERNEL_HPP
#define BD_KERNEL_HPP 0

#import "KernelBase.hpp"
#import "PitchToFrequency.hpp"

#import <AudioToolbox/AudioToolbox.h>

#import <map>
#import <vector>

#import <filur/drums/bd.h>

class BdKernel : public KernelBase {
 public:
  virtual ~BdKernel() {}

  virtual void initialize(int channelCount, double inSampleRate);
  virtual void deInitialize() {}
  virtual bool isBypassed() { return m_bypassed; }
  virtual void setBypass(bool shouldBypass) { m_bypassed = shouldBypass; }
  virtual AUAudioFrameCount maximumFramesToRender() const { return m_maxFramesToRender; }
  virtual void setMaximumFramesToRender(const AUAudioFrameCount& maxFrames);
  virtual void setMusicalContextBlock(AUHostMusicalContextBlock contextBlock) { m_musicalContextBlock = contextBlock; }
  virtual MIDIProtocolID AudioUnitMIDIProtocol() const { return kMIDIProtocol_2_0; }
  virtual void setParameter(AUParameterAddress address, AUValue value);
  virtual AUValue getParameter(AUParameterAddress address);

 protected:
  virtual bool subProcess(AudioBufferList* inBufferList,
                          AudioBufferList* outBufferList,
                          AUAudioFrameCount bufferOffset,
                          AUAudioFrameCount numFramesToProcess,
                          AUEventSampleTime timestamp);

  virtual void handleEvent(AUEventSampleTime now, AURenderEvent const* event);
  void handleParameterEvent(AUEventSampleTime now, AUParameterEvent const& parameterEvent) {}
  void handleMIDIEventList(AUEventSampleTime now,AUMIDIEventList const* midiEvent);
  void handleMIDI2VoiceMessage(const struct MIDIUniversalMessage& message);

  AUHostMusicalContextBlock m_musicalContextBlock;

  double m_sampleRate = 44100.0;

  std::map<uint64_t, double> m_parameters;

  bool m_bypassed = false;
  AUAudioFrameCount m_maxFramesToRender = 1024;

  int m_low_key = 0;
  int m_high_key = 127;
  int m_pitch = 36;
  bool m_gated = true;
  int m_currentNote = 0;
  double m_gain = 1.0;

  bd_t m_bd = {};
  bd_parameters_t m_bd_parameters = {};
  PitchToFrequency m_pitchToFrequency;
  std::vector<double> m_buffer;
};

#endif // #ifndef BD_KERNEL_HPP
