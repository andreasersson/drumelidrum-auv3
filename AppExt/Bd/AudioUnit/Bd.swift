/*
 * Copyright 2024 Andreas Ersson
 *
 * This file is part of drumelidrum-auv3.
 *
 * drumelidrum-auv3 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * drumelidrum-auv3 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with drumelidrum-auv3.  If not, see <https://www.gnu.org/licenses/>.
 */

import AVFoundation
import AudioToolbox
import CoreAudioKit
import Foundation

let kAUPresetParametersKey = "parameters"

public class BdAu: BdAudioUnit {
  private let presets: FactoryPresets = FactoryPresets()
  private var _currentPreset: AUAudioUnitPreset?

  override init(componentDescription: AudioComponentDescription, options: AudioComponentInstantiationOptions) throws {
    try super.init(componentDescription: componentDescription, options: options)
    setupParameterTree(createParameterTree())
  }

  public override var factoryPresets: [AUAudioUnitPreset]? {
    return self.presets.factoryPresets()
  }

  public override var currentPreset: AUAudioUnitPreset? {
    get { return _currentPreset }
    set {
      guard let preset = newValue else {
        _currentPreset = nil
        return
      }

      // Factory preset numbers are always >= 0.
      if preset.number >= 0 {
        if let state = self.presets.state(number: preset.number) {
          self.fullState = state
          _currentPreset = preset
        }
      } else {
        do {
          fullStateForDocument = try presetState(for: preset)
          _currentPreset = preset
        } catch {}
      }
    }
  }

  public override var supportsUserPresets: Bool {
    return true
  }

  override public var fullState: [String: Any]? {
    get {
      var state = [String: Any]()
      state[kAUPresetManufacturerKey] = self.componentDescription.componentManufacturer
      state[kAUPresetTypeKey] = self.componentDescription.componentType
      state[kAUPresetSubtypeKey] = self.componentDescription.componentSubType
      state[kAUPresetVersionKey] = self.componentVersion

      if let parameterTree = self.parameterTree {
        var parameters: [String: Any] = [:]
        for parameter in parameterTree.allParameters {
          parameters[String(parameter.address)] = parameter.value
        }
        state[kAUPresetParametersKey] = parameters
      }

      return state
    }
    set {
      guard let dictionary = newValue else { return }

      if dictionary[kAUPresetParametersKey] == nil {
        super.fullState = dictionary
        return
      }

      guard let parameters = dictionary[kAUPresetParametersKey] as? [String: Any] else { return }
      for (addressString, anyValue) in parameters {
        guard let address = UInt64(addressString), let value = anyValue as? AUValue else { continue }
        guard let parameter = self.parameterTree?.parameter(withAddress: address) else { continue }

        parameter.value = value
      }
    }
  }

  // swiftlint:disable function_body_length
  func createParameterTree() -> AUParameterTree {
    let noteRange: ClosedRange<Int> = 0...127
    let oneShotStrings = ["one shot", "gated"]
    var parameters: [AUParameterNode] = []

    parameters.append(AUParameterTree.createParameter(
      identifier: "LowKey",
      name: "low key",
      address: ParameterAddress.LowKey.rawValue,
      range: noteRange,
      defaultValue: 36,
      unit: .midiNoteNumber))

    parameters.append(AUParameterTree.createParameter(
      identifier: "HighKey",
      name: "high key",
      address: ParameterAddress.HighKey.rawValue,
      range: noteRange,
      defaultValue: 36,
      unit: .midiNoteNumber))

    parameters.append(AUParameterTree.createParameter(
      identifier: "Pitch",
      name: "pitch",
      address: ParameterAddress.Pitch.rawValue,
      range: noteRange,
      defaultValue: 36,
      unit: .midiNoteNumber))

    parameters.append(AUParameterTree.createParameter(
      identifier: "OneShot",
      name: "one shot",
      address: ParameterAddress.OneShot.rawValue,
      defaultValue: 0.0,
      strings: oneShotStrings))

    parameters.append(AUParameterTree.createParameter(
      identifier: "Tone",
      name: "tone",
      address: ParameterAddress.Tone.rawValue,
      range: 0.0...1.0,
      defaultValue: 0.25,
      unit: .ratio))

    parameters.append(AUParameterTree.createParameter(
      identifier: "Decay",
      name: "decay",
      address: ParameterAddress.Decay.rawValue,
      range: 50.0e-3...2.0,
      defaultValue: 1.0,
      unit: .seconds))

    parameters.append(AUParameterTree.createParameter(
      identifier: "Gain",
      name: "gain",
      address: ParameterAddress.Gain.rawValue,
      range: -24.0...12.0,
      defaultValue: 0.0,
      unit: .decibels))

    let global = AUParameterTree.createGroup(withIdentifier: "global", name: "global", children: parameters)
    return AUParameterTree.createTree(withChildren: [global])
  }
  // swiftlint:enable function_body_length
}
