/*
 * Copyright 2024 Andreas Ersson
 *
 * This file is part of drumelidrum-auv3.
 *
 * drumelidrum-auv3 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * drumelidrum-auv3 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with drumelidrum-auv3.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef PITCH_TO_FREQUENCY_HPP
#define PITCH_TO_FREQUENCY_HPP 0

#include <array>

class PitchToFrequency {
 public:
  PitchToFrequency();
  double frequency(int pitch) const;

 private:
  std::array<double, 128> m_table;
};

#endif // PITCH_TO_FREQUENCY_HPP
