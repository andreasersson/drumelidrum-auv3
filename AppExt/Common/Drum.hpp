/*
 * Copyright 2024 Andreas Ersson
 *
 * This file is part of drumelidrum-auv3.
 *
 * drumelidrum-auv3 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * drumelidrum-auv3 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with drumelidrum-auv3.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef DRUM_HPP
#define DRUM_HPP 0

#import <algorithm>

bool in_range(int note_nr, int low, int high) {
  return ((note_nr >= low) && (note_nr <= high));
}

bool in_range(int note_nr, int low, int high, int base_pitch, int& pitch) {
  bool note_in_range =  in_range(note_nr, low, high);
  if (note_in_range) {
    pitch = (note_nr - low) + base_pitch;
  }

  return note_in_range;
}

int limit_pitch_value(double value) {
  return std::min(127, std::max(0, static_cast<int>(value)));
}

#endif // DRUM_HPP
