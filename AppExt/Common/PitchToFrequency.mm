/*
 * Copyright 2024 Andreas Ersson
 *
 * This file is part of drumelidrum-auv3.
 *
 * drumelidrum-auv3 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * drumelidrum-auv3 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with drumelidrum-auv3.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "PitchToFrequency.hpp"

PitchToFrequency::PitchToFrequency() {
  double base_frequency = 8.1757989156437;
  const double k = pow(2.0, 1.0 / 12.0);
  double frequency = base_frequency;
  for (size_t n = 0; n < m_table.size(); ++n) {
    m_table[n] = frequency;
    frequency *= k;
  }
}

double PitchToFrequency::frequency(int pitch) const {
  if (pitch < 0) pitch = 0;
  if (pitch >= static_cast<int>(m_table.size())) {
    pitch = static_cast<int>(m_table.size()) - 1;
  }

  return m_table.at(pitch);
}
