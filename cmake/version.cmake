
# add configure dependency to git
set_property(DIRECTORY APPEND PROPERTY CMAKE_CONFIGURE_DEPENDS "${CMAKE_CURRENT_SOURCE_DIR}/.git/HEAD")
set_property(DIRECTORY APPEND PROPERTY CMAKE_CONFIGURE_DEPENDS "${CMAKE_CURRENT_SOURCE_DIR}/.git/refs/heads")
set_property(DIRECTORY APPEND PROPERTY CMAKE_CONFIGURE_DEPENDS "${CMAKE_CURRENT_SOURCE_DIR}/.git/refs/tags")

function(add_check_version_custom_target target version repository)
    set(script ${CMAKE_CURRENT_SOURCE_DIR}/cmake/check_version.cmake)
    if(NOT ${ARGV3} EQUAL "")
        set(test_version ${ARGV3})
    endif()

    set(custom_target_name ${target}_check_version)
    add_custom_target(${custom_target_name} ALL
                      WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
                      COMMAND ${CMAKE_COMMAND} -P ${script} VERSION ${version} REPOSITORY ${repository} TARGET ${CMAKE_CACHEFILE_DIR}/CMakeCache.txt TEST_VERSION ${test_version}
                      VERBATIM USES_TERMINAL
                      COMMENT "check version ${version}")
    add_dependencies(${target} ${custom_target_name})
endfunction()

function(get_git_version repository version)
    find_package(Git QUIET)
    if(Git_FOUND)
        execute_process(COMMAND ${GIT_EXECUTABLE} describe --always --dirty --match v[0-9]*.[0-9]*.[0-9]*
                        WORKING_DIRECTORY ${repository}
                        OUTPUT_VARIABLE git_describe
                        RESULT_VARIABLE git_result
                        ERROR_VARIABLE git_error
                        OUTPUT_STRIP_TRAILING_WHITESPACE)

        if(${git_result} EQUAL 0)
            set(${version} ${git_describe} PARENT_SCOPE)
        else()
            set(${version} "unknown" PARENT_SCOPE)
        endif()
    endif(Git_FOUND)
endfunction()

function(check_version repository version result_var)
    set(result 1)
    get_git_version(${repository} git_version)

    if(NOT ${ARGV3} EQUAL "")
        set(git_version ${ARGV3})
    endif()

    if(NOT "${version}" STREQUAL "${git_version}")
        set(result 0)
        message("version: ${version} != ${git_version}")
    endif()
    set(${result_var} ${result} PARENT_SCOPE)
endfunction()

function(parse_version version major_version minor_version patch_version build_version hash_version)
    string(REGEX MATCH "^v([0-9]+)\\.([0-9]+)\\.([0-9]+)-?([0-9]*)?-?(g[0-9a-fA-F]+)?-?(dirty|broken)?$" match ${version})
    if(NOT match)
        string(REGEX MATCH "^()()()()([0-9a-fA-F]+)-?(dirty|broken)?$" match ${version})
    endif()

    if(NOT CMAKE_MATCH_1)
        set(CMAKE_MATCH_1 0)
    endif()
    if(NOT CMAKE_MATCH_2)
        set(CMAKE_MATCH_2 0)
    endif()
    if(NOT CMAKE_MATCH_3)
        set(CMAKE_MATCH_3 0)
    endif()

    set(${major_version} ${CMAKE_MATCH_1} PARENT_SCOPE)
    set(${minor_version} ${CMAKE_MATCH_2} PARENT_SCOPE)
    set(${patch_version} ${CMAKE_MATCH_3} PARENT_SCOPE)
    set(${build_version} ${CMAKE_MATCH_4} PARENT_SCOPE)
    set(local_hash_version ${CMAKE_MATCH_5})
    if(NOT ${CMAKE_MATCH_6} EQUAL "")
        if(NOT ${local_hash_version} EQUAL "")
            set(local_hash_version ${local_hash_version}-)
        endif()
        set(local_hash_version ${local_hash_version}${CMAKE_MATCH_6})
    endif()
    set(${hash_version} ${local_hash_version} PARENT_SCOPE)
endfunction()

function(get_int_version variable major_version minor_version patch_version)
  math(EXPR version "${major_version} * 256*256 + ${minor_version} * 256 + ${patch_version}" OUTPUT_FORMAT DECIMAL)
  set(${variable} ${version} PARENT_SCOPE)
endfunction()

function(get_short_version variable major_version minor_version patch_version build_version hash_version)
  set(${variable} "${major_version}.${minor_version}.${patch_version}" PARENT_SCOPE)
endfunction()

function(get_long_version variable major_version minor_version patch_version build_version hash_version)
  set(version "${major_version}.${minor_version}.${patch_version}")
  if(NOT ${build_version} EQUAL "")
    set(version "${version}-${build_version}")
  endif()
  if(NOT ${hash_version} EQUAL "")
    set(version "${version}-${hash_version}")
  endif()
  set(${variable} ${version} PARENT_SCOPE)
endfunction()

function(get_version repository version_string major_version minor_version patch_version build_version hash_version)
    get_git_version(${repository} git_version)

    parse_version(${git_version} local_major_version local_minor_version local_patch_version local_build_version local_hash_version)

    set(${version_string} ${git_version} PARENT_SCOPE)
    set(${major_version} ${local_major_version} PARENT_SCOPE)
    set(${minor_version} ${local_minor_version} PARENT_SCOPE)
    set(${patch_version} ${local_patch_version} PARENT_SCOPE)
    set(${build_version} ${local_build_version} PARENT_SCOPE)
    set(${hash_version} ${local_hash_version} PARENT_SCOPE)
endfunction()

function(set_project_version major_version minor_version)
  if(NOT ${ARGV2} EQUAL "")
    set(patch_version ${ARGV2})
  endif()
  if(NOT ${ARGV3} EQUAL "")
    set(build_version ${ARGV3})
  endif()
  if(NOT ${ARGV4} EQUAL "")
    set(hash_version ${ARGV4})
  endif()

  set(PROJECT_VERSION_MAJOR ${major_version} PARENT_SCOPE)
  set(PROJECT_VERSION_MINOR ${minor_version} PARENT_SCOPE)
  set(PROJECT_VERSION_PATCH ${patch_version} PARENT_SCOPE)
  set(version_tweak ${build_version})
  if(NOT ${hash_version} EQUAL "")
    set(version_tweak ${version_tweak}-${hash_version})
  endif()
  set(PROJECT_VERSION_TWEAK ${version_tweak} PARENT_SCOPE)
endfunction()
