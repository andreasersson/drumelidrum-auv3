# AUv3 Drum synthesizers

<img src="https://gitlab.com/andreasersson/drumelidrum-auv3/-/wikis/uploads/a8b7431bd330bfaf87c673c73d718872/Bd.png" alt="bd screenshot" width="180"/>
<img src="https://gitlab.com/andreasersson/drumelidrum-auv3/-/wikis/uploads/6409854c8f04518162e3a5b63526e00d/Sd.png" alt="sd screenshot" width="180"/>
<img src="https://gitlab.com/andreasersson/drumelidrum-auv3/-/wikis/uploads/cc0446a3dbeb1edc204228f048fc8d2c/Tom.png" alt="tom screenshot" width="180"/>
<img src="https://gitlab.com/andreasersson/drumelidrum-auv3/-/wikis/uploads/8164171310a5dddf0215c8db3a9927ab/Clap.png" alt="clap screenshot" width="180"/>

<img src="https://gitlab.com/andreasersson/drumelidrum-auv3/-/wikis/uploads/b126772501629bf416fa6d3f16b5ac16/Hihat.png" alt="hihat screenshot" width="240"/>
<img src="https://gitlab.com/andreasersson/drumelidrum-auv3/-/wikis/uploads/8bc4b7e5812b94375d0b1a02321e1b1a/Cymbal.png" alt="cymbal screenshot" width="240"/>

## Requirements
- [XCode]
- [CMake][CMake] 3.28 or later.

## Dependencies
- [filur][filur]
- [auv3][auv3]

## Build for MacOS.
Download the source or clone the git repository.

    git clone https://gitlab.com/andreasersson/drumelidrum-auv3.git

*Note that it is currently only possible to build using Xcode as a generator.*

    cmake -S drumelidrum-auv3 -B build-drumelidrum-auv3 -G Xcode
    cmake --build build-drumelidrum-auv3 --config Release

### Code signing
By default, the application, application extension and the framework will be singed using ad hoc signing.  
If you want to sign the build using you own certificate, set CMAKE_XCODE_ATTRIBUTE_DEVELOPMENT_TEAM and CMAKE_XCODE_ATTRIBUTE_CODE_SIGN_IDENTITY on the command line during configuration.  
example:

    cmake -DCMAKE_XCODE_ATTRIBUTE_CODE_SIGN_IDENTITY="Apple Development" -DCMAKE_XCODE_ATTRIBUTE_DEVELOPMENT_TEAM="A1B2C3D4E5" -S drumelidrum-auv3 -B build-drumelidrum-auv3 -G Xcode

## Installation
*Note that the build system will automatically register the application and the application extensions in the system.*

Start the container application, Drumelidrum, to register the application extension in the system.

## License
[![GPLv3](https://www.gnu.org/graphics/gplv3-with-text-136x68.png "GNU General Public License")](https://www.gnu.org/licenses/gpl.html)

    drumelidrum-auv3 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    drumelidrum-auv3 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

[Xcode]: https://developer.apple.com/xcode/
[CMake]: https://cmake.org/
[filur]: https://gitlab.com/andreasersson/filur
[auv3]: https://gitlab.com/andreasersson/auv3
